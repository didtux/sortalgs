public class ProcessedData//this class saves in an organized way the data to be used as output
{
	public String sortName;
	
	public double minTime;
	public double maxTime;
	public double avgTime;

	public int minMovements;
	public int maxMovements;
	public int avgMovements;

	public int minComparisons;
	public int maxComparisons;
	public int avgComparisons;


	public ProcessedData(String name,ArrayList<Double> allTimes,ArrayList<Double> allMovements,ArrayList<Double>allComparisons)
	{
		sortName = name;

		minTime = getMin(allTimes);
		maxTime = getMax(allTimes);
		avgTime = getAvg(allTimes);

		minMovements = getMin(allMovements);
		maxMovements = getMax(allMovements);
		avgMovements = getAvg(allMovements);

		minComparisons = getMin(allComparisons);
		maxComparisons = getMax(allComparisons);
		avgComparisons = getAvg(allComparisons);
	}

	public double getMax(ArrayList<Double> set)
	{
		double toReturn=set.get(0);
		for(double item:set)
		{
			if (item > toReturn)
			{
				toReturn=item;
			}
		}
	}
	public double getMin(ArrayList<Double> set)
	{
		double toReturn=set.get(0);
		for(double item:set)
		{
			if(item < toReturn)
			{
				toReturn=item;
			}
		}
	}
	public double getAvg(ArrayList<Double> set)
	{
		double toReturn=0;
		for(double item:set)
		{
			toReturn+=item
		}
		return toReturn /= set.size();
	}

	public void printData()
	{
		System.out.println("algorithm: " + sortName);
		System.out.println("\t min \t max \t avg");
		System.out.println("time \t" + minTime +"\t"+ maxTime +"\t"+ avgTime);
		System.out.println("movements \t" + minMovements +"\t"+ maxMovements +"\t"+ avgMovements);
		System.out.println("comparisons \t" + minComparisons +"\t"+ maxComparisons +"\t"+ avgComparisons);
	}
}