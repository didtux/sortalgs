import java.util.ArrayList;
import java.util.Random;

public abstract class SortAlgs
{
	public int iterations;// # of times to be executed each algorithm
	public int setSize;// # of elements to sort
	public int maxVal;// # of max number of the set
	public ArrayList<Double> execTime;
	public ArrayList<Double> movements;
	public ArrayList<Double> comparisons;

	public void execute(int a,int b,int c)
	{
		execTime=new ArrayList<Double>();
		movements=new ArrayList<Double>();
		comparisons=new ArrayList<Double>();
		iterations= a;
		setSize= b;
		maxVal= c;

		for(int i=0; i<iterations; i++)
		{
			addStats(runIt(getSet(setSize,maxVal)));
		}
	}
	
	public double[] getSet(int items, int max)
	{
		double[] toReturn = new double[items];
		Random generator = new Random(System.currentTimeMillis());
		for(int i=0;i<items;i++)
		{
			toReturn[i] = generator.nextDouble()*max;
		}
		return toReturn;
	}
	public abstract double[] runIt(double[] set);//executes one time a sort algorithm and returns the stats
	public void addStats(double[] toAdd) //fetch the stats from runIt and append them into the ArrayLists
	{
		execTime.add(toAdd[0]);
		movements.add(toAdd[1]);
		comparisons.add(toAdd[2]);
		
	}
	public abstract ProcessedData getdata(); //return the processed statistical 


}
