public class BubbleSort extends SortAlgs
{

	public BubbleSort(int one,int two, int three)
	{
		execute(o
			ne,two,three);
	}
	public double[] runIt(double[] set)
	{
		double[] toReturn= new double[3];
		double initialTime = System.currentTimeMillis();
		toReturn[1] = toReturn[2] = 0;
		
		for(int i=setSize-1;i>0;i--)
		{
			for(int j=0;j<i;j++)
			{
				toReturn[2]++;
				if(set[j] > set[j+1])
				{
					double temp = set[j];
					set[j] = set[j+1];
					set[j+1] = temp;
					toReturn[1] += 2;
				}
			}
		}
		
		toReturn[0] = System.currentTimeMillis() - initialTime;
		return toReturn;
	}
	
	public ProcessedData getData()
	{
		return new ProcessedData("Bubble Sort",  execTime, movements, comparisons);
	}
}
